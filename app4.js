let firstNumber = Number(prompt('Enter first number, please'))
while (!firstNumber || isNaN(firstNumber)) {
    firstNumber = Number(prompt('Enter first number, please'))
}

let secondNumber = Number(prompt('Enter second number, please'))
while (!secondNumber || isNaN(secondNumber)) {
    secondNumber = Number(prompt('Enter second number, please'))
}

let operation = prompt('Enter type of operation: difference(-), sum(+), division(/), multiplication(*)')
while (operation != '-' && operation != '+' && operation != '/' && operation != '*') {
    operation = prompt('Enter type of operation: difference(-), sum(+), division(/), multiplication(*)') 
}

function calc(a, b, operation) {
    if(operation === '-') {
        return a - b
    }
    if(operation === '+') {
        return a + b
    }
    if(operation === '/') {
        return a / b
    }
    if(operation === '*') {
        return a * b
    }
} 

console.log(calc(firstNumber, secondNumber, operation)) 